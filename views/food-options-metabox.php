<?php
/**
 * File defines metabox controls  *
 *
 * @package erp_hr_food
 * @author Talha Ahmed <talha.ahmed@gmail.com>
 */

$data = get_post_meta( $post->ID, $this->post_type . '_data', true );

if ( ! isset( $data['mealtime'] ) ) {
    $data['mealtime'] = 'Dinner';
}
if ( ! isset( $data['helpings'] ) ) {
    $data['helpings'] = '1';
}
if ( ! isset( $data['vendor'] ) ) {
    $data['vendor'] = '';
}
if ( ! isset( $data['available'] ) ) {
    $data['available'] = 'yes';
}

$mealtime = explode( ',', $data['mealtime'] );
?>

<div class="form-group">
    <label for="food_mealtime">Meal Times</label>
    <div>
        <?php foreach ( $this->meals as $meal => $obj ) {?>
        <div>
            <label class="form-control">

                <input type="hidden"
                        name="<?php echo 'food_' . esc_html( $meal ); ?>"
                        value="no">

                <input type   = "checkbox"
                        class = "text"
                        value = "yes"
                        name  = "<?php echo 'food_' . esc_html( $meal ); ?>"
                        id    = "<?php echo 'food_' . esc_html( $meal ); ?>"
                        <?php echo in_array( $meal, $mealtime )? 'checked': ''; ?> >

                <?php echo esc_html( $obj->get_title() ); ?>
            </label>
        </div>
        <?php } ?>
    </div>
</div>

<div class="form-group">
    <label for="food_helpings">Helpings Per Order</label>
    <input
            type  = "number"
            class = "form-control"
            value = "<?php echo esc_html( $data['helpings'] ); ?>"
            name  = "food_helpings"
            min   = 1
            max   = 4
            id    = "food_helpings" >
</div>

<div class="form-group">
    <label for=food_vendor>Vendor</label>
    <input
            type  = "text"
            class = "form-control"
            value = "<?php echo esc_html( $data['vendor'] ); ?>"
            name  = "food_vendor"
            id    = "food_vendor" >
</div>

<div class="form-group">
    <label for="food_available">
        <input type="hidden" name="food_available" value="no">
        <input
                class = "form-control"
                type  = "checkbox"
                value = "yes"
                name  = "food_available"
                id    = "food_available"
                <?php echo 'yes' === $data['available'] ? 'checked': ''; ?> >
        Available
    </label>
</div>

