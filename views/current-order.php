<?php
/**
 * Facilitates ordering of food if the user is eligible and a mealtime is
 * open.  User can cancel current order if an order is already active.  HR
 * manager can order on behalf of anyone.
 *
 * @author Talha Ahmed
 * @version $Id$
 * @copyright Talha Ahmed, 30 May, 2017
 * @package default
 */

$can_order = $this->current_user_can_order();
$current_meal = $this->get_current_mealtime();
$user_name = $this->get_user_name();
$current_order = $this->get_current_order();
global $erp_hr_food_plugin_instance;
$post_type = $erp_hr_food_plugin_instance->food->get_post_type();
$current_menu = $current_meal->get_menu( $post_type );
?>
<br>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Current Meal</h3>
    </div>
    <div class="panel-body">
        <dl>
            <dt>
                <?php echo $current_meal->get_desc(); ?>
            </dt>
            <dd>
                <?php echo $current_meal->get_time_diff_desc(); ?>
            </dd>
        </dl>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Current Order</h3>
    </div>
    <div class="panel-body">

        <?php if ( null !== $current_order ) { ?>

        <div id='current-order'>
            <?php include WPERP_FOOD_VIEWS . '/food-order-form.php'; ?>
        </div>

        <?php } elseif ( $can_order ) { ?>

        <div id="eligible">
            <?php if ( count( $current_menu ) >= 1 ) {?>
            <?php include WPERP_FOOD_VIEWS . '/food-order-form.php'; ?>
            <?php } else { ?>
            <h4>
                No menu available!
            </h4>
            <?php } ?>
        </div>

        <?php } else { ?>

        <div id="not-eligible">
            <dl>
                <dt class="text-danger">
                    You are NOT Eligible to order as <span class=badge><?php echo $user_name; ?></span>
                </dt>
                <dd>
                    <?php echo $this->reason; ?>
                </dd>
            </dl>
        </div>

        <?php } // End if(). ?>

    </div>
</div>

