<?php ?>
<ul class="list-group dl-horizontal">
    <li class="list-group-item"><dt style="margin-bottom:0;line-height:1"> MEALTIME_I18N </dt> <dd style="margin-bottom:0;line-height:1">MEAL_TIME_PH</dd> </li>
    <li class="list-group-item"><dt style="margin-bottom:0;line-height:1">HELPINGS_I18N</dt> <dd style="margin-bottom:0;line-height:1">HELPINGS_PH</dd> </li>
    <li class="list-group-item"><dt style="margin-bottom:0;line-height:1">VENDOR_I18N</dt> <dd style="margin-bottom:0;line-height:1">VENDOR_PH</dd> </li>
    <li class="list-group-item"><dt style="margin-bottom:0;line-height:1">AVAILABLE_I18N</dt> <dd style="margin-bottom:0;line-height:1">AVAILABLE_PH</dd> </li>
    <li class="list-group-item"><dt style="margin-bottom:0;line-height:1">ORDERCNT_I18N</dt> <dd style="margin-bottom:0;line-height:1">ORDERCNT_PH</dd></li>
    <li class="list-group-item"><dt style="margin-bottom:0;line-height:1">RATING_I18N
        <span class='badge'> RATINGCNT_PH RATINGCNT_I18N </span></dt>
        <dd style="margin-bottom:0;line-height:1">
            <div id="food_rating"
                class="rateit"
                data-rateit-resettable="false"
                data-food_post_id="ERP_HR_FOOD_ID"
                data-rateit-value="FOOD_RATING"
                data-rateit-readonly="READONLY_PH"
                data-rateit-ispreset="true">
            </div>
        </dd>
    </li>
</ul>
