<?php
/**
 * Admin Listing info for single employee
 *
 * @author Talha Ahmed
 * @copyright Talha Ahmed, 26 May, 2017
 * @package erp_hr_food
 */

$meals = ICE\ERP\HRM\get_mealtimes();
global $erp_hr_food_plugin_instance;
?>
<br>

<ul data-tabs="tabs" class="nav nav-tabs" id="erp_hr_food_mealtime_tabs">
    <?php $count = 0; ?>
    <?php foreach ( $meals as $meal => $meal_obj ) {?>
        <li>
            <a data-toggle="tab"
                <?php echo 0 === $count++ ? ' class="active" ' : '' ?>
                href="#<?php echo 'erp_hr_food_mealtime_tab_' . esc_html( $meal ) ?>">
                <?php echo esc_html( $meal_obj->get_title() ) ?>
            </a>
        </li>
    <?php }?>
</ul>

<div class="tab-content" id="erp_hr_food_mealtime_tab_content">
    <?php $count = 0; ?>
    <?php foreach ( $meals as $meal => $meal_obj ) {?>
    <div 
        class="tab-pane <?php echo 0 === $count++ ? ' active' : '' ?>"
            id="<?php echo 'erp_hr_food_mealtime_tab_' . esc_html( $meal ) ?>">

        <div class='panel panel-default'>
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php echo esc_html( $meal_obj->get_title() . __( ' Information', 'erp-food' ) ); ?> 
                </h3>
            </div>
            <div class='panel-body'>
                <ul class="list-group dl-horizontal">
                    <?php foreach ( ICE\ERP\HRM\Mealtime::$keys as $key ) { ?>
                    <li class="list-group-item col-md-3">
                        <dt style="margin-bottom:0;line-height:1">
                            <?php echo esc_html( $key );?>
                        </dt>
                        <dd style="margin-bottom:0;line-height:1">
                            <?php echo esc_html( $meal_obj->get( $key ) ); ?>
                        </dd>
                    </li>
                    <?php } ?>
                </ul>
            </div>
        </div>

        <div class='panel panel-default'>
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php echo esc_html( $meal_obj->get_title() . __( ' Menu ', 'erp-food' ) ); ?> 
                </h3>
            </div>
            <div class='panel-body'>
                <?php
                $post_type = $erp_hr_food_plugin_instance->food->get_post_type();
                $menu = $meal_obj->get_menu( $post_type );
                if ( count( $menu ) ) {
                ?>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <?php foreach ( $menu as $food ) { ?>
                        <tr>
                            <th>
                                <a href="<?php echo esc_html( get_the_permalink( $food['post'] ) );?>" >
                                    <?php echo esc_html( get_the_title( $food['post'] ) ); ?>
                                </a>
                            </th>
                            <td>
                                <?php echo $food[ 'post' ]->post_content; ?>
                            </td>
                            <td>
                                <?php echo __( 'Ordered ', 'erp-food'  ) . $food[ 'meta_data' ][ 'orders' ] . __( ' times', 'erp-food' ); ?>
                            </td>
                            <td>
                                <span id="erp_hr_food_rating_<?php echo esc_html( $food['post']->ID ); ?>"
                                    class="rateit"
                                    data-rateit-resettable="false"
                                    data-food_post_id="<?php echo $post_type . '_' . $meal . '_' . $food['post']->ID; ?>"
                                    data-rateit-value="<?php echo $food[ 'meta_data' ][ 'rating' ] ?>"
                                    data-rateit-readonly="true"
                                    data-rateit-ispreset="true">
                                    <div class="badge">
                                        <?php echo $food[ 'meta_data' ][ 'rating_count' ] . __( ' ratings', 'erp-food' ); ?>
                                    </div>
                                </span>
                            </td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
                <?php } else {
                    echo esc_html__( 'No Foods Found', 'erp-food' );
                } ?>
            </div>
        </div>

    </div>
    <?php }; // End foreach(). ?>
</div>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('#erp_hr_food_mealtime_tabs').tab();
    });
</script> 
