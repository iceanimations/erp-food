<?php /** Food order form. */ ?>
<div class="row">
    <?php if ( $current_order ) { ?>
    <h4 style='margin:20px 20px 20px 20px' class="well text-info">
        You already have an active order placed!
    </h4>
    <?php } else { ?>
    <h4 style='margin:20px 20px 20px 20px' class="well text-info">
        Let's order Foooood!
    </h4>
    <?php } ?>
    <p style="margin:8px">
        <strong>
            <?php echo 'Choose from ' . count( $current_menu ) . ' dishes'; ?>
        </strong>
    </p>
    <div class="table-responsive">
        <table class="table table-striped">
            <?php $first = true; ?>
            <?php $check = false; ?>
            <?php foreach ( $current_menu as $food ) { ?>
                <?php if ( $current_order ) {
                    $check = (int) $current_order->food_id === (int) $food['post']->ID;
                } else {
                    $check = $first;
                } ?>
            <tr>
                <td style="vertical-align:middle">
                    <div class="radio">
                        <label>
                            <input type="radio" value="<?php echo $food['post']->ID ?>" name="erp_hr_food_id" id="food_id_<?php echo $food['post']->ID ?>" <?php echo $check ? 'checked': '' ?>>
                            <?php if ( $first ) {
                                $first = false;
                            } ?>
                        </label>
                    </div>
                </td>
                <td style="vertical-align:middle">
                    <?php echo esc_html( get_the_title( $food['post'] ) ); ?>
                </td>
                <td style="vertical-align:middle">
                    <?php echo esc_html( $food[ 'post' ]->post_content ); ?>
                </td style="vertical-align:middle">
                <td style="vertical-align:middle">
                    <?php echo __( 'Ordered ', 'erp-food'  ) . $food[ 'meta_data' ][ 'orders' ] . __( ' times', 'erp-food' ); ?>
                </td style="vertical-align:middle">
                <td style="vertical-align:middle">
                    <span id="erp_hr_food_rating_<?php echo esc_html( $food['post']->ID ); ?>"
                        class="rateit"
                        data-rateit-resettable="false"
                        data-food_post_id="<?php echo $post_type . '_' . $current_meal->get_id() . '_' . $food['post']->ID; ?>"
                        data-rateit-value="<?php echo $food[ 'meta_data' ][ 'rating' ] ?>"
                        data-rateit-readonly="true"
                        data-rateit-ispreset="true">
                        <div class="badge">
                            <?php echo $food[ 'meta_data' ][ 'rating_count' ] . __( ' ratings', 'erp-food' ); ?>
                        </div>
                    </span>
                </td>
            </tr>
            <?php } // End foreach(). ?>
        </table>
        <button style="margin:8px" class="btn btn-default btn-primary" type="submit" id="erp_hr_food_place_order" value="Order Food" user_id="<?php echo $this->user_id ?>" mealtime="<?php echo $current_meal->get_id(); ?>" date="<?php echo current_time( 'Y-m-d' ) ?>">
            <?php echo $current_order ? 'Modify': 'Order'; ?> <?php echo $current_meal->get_title(); ?> for <?php echo $user_name; ?>
        </button>
        <?php if ( $current_order ) { ?>
        <button style="margin:8px" class="btn btn-default btn-primary" type="submit" id="erp_hr_food_cancel_order" value="Cancel Food" user_id="<?php echo $this->user_id ?>" mealtime="<?php echo $current_meal->get_id(); ?>" date="<?php echo current_time( 'Y-m-d' ) ?>">
            Cancel Order
        </button>
        <?php } ?>
        <div style='margin:20px 20px 20px 20px' id='erp_hr_food_submission_result'></div>
    </div>
</div>
