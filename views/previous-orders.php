<?php
/**
 * Show previous orders made by an employee in a single employee view. Also
 * allow user to comment and rate the meal.
 *
 * @author Talha Ahmed
 * @copyright ICE Animations 2017
 * @package erp_hr_food
 */

$orders = $this->get_orders();
?>
<br>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
            <?php echo esc_html( "Order history for {$this->get_user_name()} " );  ?>
        </h3>
    </div>
</div>
<div class="panel-body">
    <div class="table-responsive table">
        <table class="table table-striped">
            <tr>
                <th>Date</th>
                <th>Is Active?</th>
                <th>Meal Time</th>
                <th>Food Ordered</th>
                <th>Rating</th>
                <th>Comment</th>
                <th></th>
            </tr>
        <?php foreach ( $orders as $order ) { ?>
            <?php $allow_rating = $this->viewing_current_user() && ! $order->is_active(); ?>
            <tr>
                <td>
                    <?php echo esc_html( $order->date ); ?>
                </td>
                <td>
                    <?php echo $order->is_active() ? 'active': ''; ?>
                </td>
                <td>
                    <?php echo esc_html( $order->mealtime ); ?>
                </td>
                <td>
                    <?php $food_post = get_post( $order->food_id ) ?>
                    <?php $comment_text = $order->comment_id ? get_comment_text( $order->comment_id ): ''; ?>
                    <a href="<?php echo esc_html( get_the_permalink( $food_post ) );?>" >
                        <?php echo esc_html( get_the_title( $food_post ) ); ?>
                    </a>
                </td>
                <td>
                    <span
                        id="erp_hr_food_order_rating_<?php echo esc_html( $order->id ); ?>"
                        class="rateit"
                        data-rateit-resettable = "false"
                        data-food_order_id     = "<?php echo esc_html( $order->id ); ?>"
                        data-rateit-value      = "<?php echo esc_html( $order->rating ); ?>"
                        data-rateit-readonly   = "true"
                        data-rateit-ispreset   = "true" >
                    </span>
                </td>
                <td>
                    <div
                        id="erp_hr_food_order_comment_<?php echo esc_html( $order->id ); ?>"
                        data-food_order_id     = "<?php echo esc_html( $order->id ); ?>" >
                        <?php echo esc_html( $comment_text ); ?>
                    </div>
                </td>
                <td>
                    <?php if ( $allow_rating ) { ?>
                    <button
                        class="btn-default btn-small"
                        name="erp_hr_food_order_button"
                        data-food_order_id="<?php echo esc_html( $order->id ) ?>"
                        data-toggle="modal"
                        data-target="#erp_hr_food_rate_modal_<?php echo esc_html( $order->id ); ?>">
                        Rate >>
                    </button>
                    <?php } ?>
                </td>
            </tr>

            <?php if ( $allow_rating ) { ?>
            <div class="modal" id="erp_hr_food_rate_modal_<?php echo esc_html( $order->id ); ?>" role="dialog">
                <div class="modal-content">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-header">
                            <button data-dismiss="modal" class="close">&times;</button>
                            <h3 class="modal-title">Rate Your Order</h3>
                        </div>
                        <div class="modal-body">
                            <dl class="dl-horizontal">
                                <dt>Date Ordered</dt>
                                <dd><?php echo esc_html( $order->date ); ?></dd>
                                <dt>Meal time</dt>
                                <dd><?php echo esc_html( $order->mealtime ); ?></dd>
                                <dt>Meal Ordered</dt>
                                <dd> <?php echo esc_html( get_the_title( $food_post ) ) ?></dd>
                                <dt>Your Rating</dt>
                                <dd>
                                    <span
                                        id="erp_hr_food_order_rating_new_<?php echo esc_html( $order->id ); ?>"
                                        class="rateit"
                                        data-rateit-resettable = "true"
                                        data-food_order_id     = "<?php echo esc_html( $order->id ); ?>"
                                        data-rateit-value      = "<?php echo esc_html( $order->rating ); ?>"
                                        data-rateit-readonly   = "false"
                                        data-rateit-ispreset   = "true" >
                                    </span>
                                </dd>
                                <dt>Your Comments</dt>
                                <dd>
                                    <textarea id="erp_hr_food_order_comment_new_<?php echo esc_html( $order->id ); ?>"><?php echo esc_html( $comment_text ); ?></textarea>
                                </dd>
                            </dl>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default erp_hr_food_order_dismiss_modal" data-dismiss="modal"
                                id="erp_hr_food_order_dismiss_modal"
                                data-food_order_id=<?php echo esc_html( $order->id ); ?> >
                                Close
                            </button>
                            <button type="button" class="btn btn-primary erp_hr_food_order_submit_rating"
                                id="erp_hr_food_order_submit_rating"
                                data-food_order_id=<?php echo esc_html( $order->id ); ?> >
                                Save changes
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <?php } // End if(). ?>

        <?php } // End foreach(). ?>
        </table>
    </div>
</div>
