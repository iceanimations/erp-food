<?php
/**
 * Uninstall ERP Food plugin
 *
 * @author Talha Ahmed
 * @copyright ICE Animations 02 June, 2017
 * @package erp_hr_food
 */

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
    exit();
}

global $wpdb;
$drop = 'DROP TABLE IF EXISTS ' . $wpdb->prefix . 'erp_hr_food_order;';
$wpdb->query( $drop );
