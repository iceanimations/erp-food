<?php
/**
 * File containing Food_Orders Class
 *
 * @package erp_hr_food
 * @author Talha Ahmed
 */

namespace ICE\ERP\HRM;

use WeDevs\ERP\HRM\Models\EmployeeExtended as EmployeeExtended;


/**
 * Class Food_Orders
 *
 * @author Talha Ahmed
 */
class Food_Orders {

    /**
     * User id of the profile currently being viewed
     *
     * @var string
     */
    var $user_id;

    /**
     * Constructor for Food_Orders
     */
    function __construct() {
        $this->user_id = filter_input( INPUT_GET, 'id' );
        $this->tabs = $this->get_tabs();
        $this->eligible = false;
        $this->emp = null;
        $this->current_meal = null;
        $this->reason = 'No checks on eligibility were made';
        foreach ( $this->tabs as $tab => $settings ) {
            add_action( 'erp_hr_food_tab_' . $tab, $settings['callback'] );
        }
        $this->output();
    }

    /**
     * Output the Food Ordering HTML
     */
    private function output() {
        $this->create_tabs();
        $this->create_tabs_content();
        $this->enable_tabs();
    }

    /**
     * Create Tabs
     */
    private function create_tabs() {
        $count = 0;
        ?>
        <ul data-tabs="tabs" class="nav nav-tabs" id="erp_hr_food_tabs">
            <?php foreach ( $this->tabs as $tab => $ts ) {?>
                <li>
                    <a data-toggle="tab"
                        <?php echo 0 === $count++ ? ' class="active" ' : '' ?>
                        href="#<?php echo 'erp_hr_food_tab_' . esc_html( $tab ) ?>">
                        <?php echo esc_html( $ts['title'] ) ?>
                    </a>
                </li>
            <?php }?>
        </ul>
        <?php
    }

    /**
     * Create Content for Food Order Tabs
     */
    private function create_tabs_content() {
        $count = 0;
        ?>
        <div class="tab-content" id="erp_hr_food_tab_content">
            <?php foreach ( $this->tabs as $tab => $ts ) {?>
            <div 
                class="tab-pane <?php echo 0 === $count++ ? ' active' : '' ?>"
                    id="<?php echo 'erp_hr_food_tab_' . esc_html( $tab ) ?>">
                <?php do_action( 'erp_hr_food_tab_' . $tab ) ?>
            </div>
            <?php }?>
        </div>

        <?php
    }

    /**
     * Put JS to enable bootstrap tabs
     */
    private function enable_tabs() {
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $('#erp_hr_food_tabs').tab();
            });
        </script> 
        <?php
    }

    /**
     * Return All subtabs and callbacks for.
     *
     * @return array
     */
    private function get_tabs() {
        $tabs = array(
            'new_order'     => array(
                'title'     => __( 'Current Order', 'erp-food' ),
                'callback'  => array( $this, 'current_order' )
            ),
            'previous_orders' => array(
                'title'     => __( 'Order History', 'erp-food' ),
                'callback'  => array( $this, 'previous_orders' )
            ),
            'info'     => array(
                'title'     => __( 'Meal Times', 'erp-food' ),
                'callback'  => array( $this, 'meal_times' )
            ),
        );
        return apply_filters( 'erp_hr_single_employee_food_tabs', $tabs );
    }

    /**
     * Verifies if the current user is the one viewing his own profile.
     */
    public function viewing_current_user() {
        $current_user_id = get_current_user_id();
        return (int) $current_user_id === (int) $this->user_id;
    }

    /**
     * Get an employee object of the currently viewed employee
     */
    public function get_employee() {
        return EmployeeExtended::where( 'user_id', '=', $this->user_id )->first();
    }

    /**
     * Get current mealtime
     */
    public function get_current_mealtime() {
        if ( null === $this->current_meal ) {
            $this->current_meal = get_current_mealtime();
        }
        return $this->current_meal;
    }

    /**
     * Get todays attendance
     */
    public function get_todays_attendance() {
        $emp = $this->get_employee();

        if ( null === $emp ) {
            $this->reason = 'The user is not an employee';
            return null;
        }
        $this->emp = $emp;
        $att = $emp->attendance();

        $att_today = $att
            ->where( 'date', current_time( 'Y-m-d' ) )
            ->where( 'shift_title', '' );

        return $att_today->first();
    }

    /**
     * Get order for the current meal time.
     */
    public function get_current_order() {
        $mealtime = $this->get_current_mealtime();

        return Models\Food_Order::whereRaw(
            'user_id = ? and date = ? and mealtime = ?',
            array(
                $this->user_id,
                current_time( 'Y-m-d' ),
                $mealtime->get_id()
            )
        )->first();
    }

    /**
     * Get all orders.
     *
     * @param int $num Number of orders to retrieve.
     */
    public function get_orders( $num = 20 ) {
        return Models\Food_Order::where( 'user_id', $this->user_id )
            ->orderBy( 'id', 'desc' )
            ->take( $num )
            ->get();
    }

    /**
     * Check Eligibility of the user being viewed for the currently_active
     * meal
     */
    public function check_eligibility() {
        $eligible = false;
        $att = $this->get_todays_attendance();

        if ( null !== $att ) {
            $checkin = $att->checkin;
            $checkout = $att->checkout;
            $current_meal = $this->get_current_mealtime();
            $now = strtotime( current_time( 'H:i' ) );

            // should be in at this time.
            if ( null === $checkout || $now < strtotime( $checkout ) ) {
                // either checkin is undefined or eligible for meal.
                if ( null === $checkin ) {
                    if ( 'yes' === $att->present ) {
                        $eligible = true;
                        $this->reason = '';
                    } else {
                        $this->reason = 'The user is not marked PRESENT';
                    }
                } elseif ( $current_meal->is_eligible( $checkin, 'mysql' ) ) {
                    $eligible = true;
                    $this->reason = '';
                } else {
                    $this->reason = "The user's in time {$checkin} is later
                                    than {$current_meal->get_title()}'s criterion
                                    {$current_meal->get('criterion')}";
                }
            } else {
                $this->reason = 'The user is not marked IN';
            }
        } elseif ( null !== $this->emp ) {
            $this->reason = 'The user is not marked present';
        }
        $this->eligible = $eligible;
        return $eligible;
    }

    /**
     * Get User Name
     */
    private function get_user_name() {
        $user_data = get_userdata( $this->user_id );
        $user_name = $user_data->first_name . ' ' . $user_data->last_name;
        if ( ' ' === $user_name ) {
            $user_name = $user_data->user_login;
        }
        return $user_name;
    }

    /**
     * Check if currently logged in user can order for the user whose profile
     * is being used.
     *
     * @param mixed $eligible if the user is eligible.
     */
    public function current_user_can_order( $eligible = null ) {
        if ( null === $eligible ) {
            $eligible = $this->check_eligibility();
        }
        $current_meal = $this->get_current_mealtime();
        if ( $current_meal->is_open() ) {
            if ( $eligible ) {
                if ( $this->viewing_current_user() || current_user_can( 'erp_hr_manager' ) ) {
                    return true;
                } else {
                    $this->reason = 'Only HR Managers can order on behalf of other employees';
                }
            }
        } else {
            $this->reason = $current_meal->get_title() . ' is not open';
        }
        return false;
    }

    /**
     * Tab for showing current meal rules
     */
    public function meal_times() {
        include WPERP_FOOD_VIEWS . '/mealtimes-info.php';
    }

    /**
     * Tab for Ordering New Food
     */
    public function current_order() {
        include WPERP_FOOD_VIEWS . '/current-order.php';
    }

    /**
     * Tab for showing, rating and commenting on previous orders
     */
    public function previous_orders() {
        include WPERP_FOOD_VIEWS . '/previous-orders.php';
    }
}; // End Class.

new Food_Orders();
