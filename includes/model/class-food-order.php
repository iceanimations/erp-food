<?php
/**
 * Contains the Food_Order Model Class
 *
 * @package erp_hr_food
 * @author  Talha Ahmed <talha.ahmed@iceanimations.com>
 * @license Unlicensed http://www.iceanimations.com
 * @link    http://www.iceanimations.com
 */

namespace ICE\ERP\HRM\Models;

use WeDevs\ERP\Framework\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class class-food-order
 *
 * @author Talha Ahmed
 */
class Food_Order extends Model {
    use SoftDeletes;

    /**
     * Table Name for the Model.
     *
     * @var string
     */
    protected $table = 'erp_hr_food_order';

    /**
     * Fillable fields of the model for bulk updates
     *
     * @var string
     */
    protected $fillable = [ 'user_id', 'date', 'food_id', 'mealtime', 'rating', 'comment' ];

    /**
     * Let ORM Manage timestamps such as created_at and deleted_at
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Check if current order is active
     */
    public function is_active() {
        $mealtime = \ICE\ERP\HRM\get_mealtimes()[$this->mealtime];
        return $this->is_today() && $mealtime->is_open();
    }

    /**
     * Was order placed for today
     */
    public function is_today() {
        return current_time( 'Y-m-d' ) === $this->date ;
    }
}
