<?php
/**
 * This file contains some mealtime functions
 *
 * @package erp_hr_food
 * @author Talha Ahmed <talha.ahmed@gmail.com>
 */
namespace ICE\ERP\HRM;

require WPERP_FOOD_INCLUDES . '/class-mealtime.php';

/**
 * Get default mealtimes
 *
 * @return array
 */
function get_mealtimes_with_defaults() {
    return array (
        'breakfast'      => array(
            'title'      => __( 'Breakfast', 'erp-food' ),
            'defaults'  => array(
                'enabled'    => 'yes',
                'criterion'  => '02:00',
                'open_time'  => '06:00',
                'close_time' => '08:00'
            )
        ),
        'lunch'          => array(
            'title'      => __( 'Lunch', 'erp-food' ),
            'defaults'   => array(
                'enabled'    => 'yes',
                'criterion'  => '05:00',
                'open_time'  => '11:00',
                'close_time' => '13:00'
            )
        ),
        'iftar'          => array(
            'title'      => __( 'Iftar', 'erp-food' ),
            'defaults' => array(
                'enabled'    => 'yes',
                'criterion'  => '17:00',
                'open_time'  => '17:00',
                'close_time' => '19:00'
            )
        ),
        'dinner'         => array(
            'title'      => __( 'Dinner', 'erp-food' ),
            'defaults' => array(
                'enabled'    => 'yes',
                'criterion'  => '12:00',
                'open_time'  => '20:00',
                'close_time' => '21:00'
            )
        ),
        'sehri'          => array(
            'title'      => __( 'Sehri', 'erp-food' ),
            'defaults'   => array(
                'enabled'    => 'yes',
                'criterion'  => '02:00',
                'open_time'  => '02:00',
                'close_time' => '03:00'
            )
        ),
    );
}

/**
 * Compare Mealtimes by open times
 *
 * @param mixed  $meal1 First meal.
 * @param mixed  $meal2 Second meal.
 * @param string $key according to which the mealtimes are to be compared.
 */
function compare_mealtimes( $meal1, $meal2, $key = 'close_time' ) {
    $t1 = strtotime( $meal1->get( $key ) );
    $t2 = strtotime( $meal2->get( $key ) );
    if ( $t1 === $t2 ) {
        return 0;
    }
    return ( $t1 < $t2 ) ? -1 : 1;
}

/**
 * Get Meal Settings from options
 *
 * @return array
 */
function get_mealtimes() {

    $meals_with_defaults = get_mealtimes_with_defaults();
    $meals = array();
    foreach ( $meals_with_defaults as $meal => $info ) {
        $title = $info['title'];
        $defaults = $info['defaults'];
        $meals[ $meal ] = new Mealtime( $meal, $title, $defaults );
    }
    uasort( $meals, 'ICE\ERP\HRM\compare_mealtimes' );
    return $meals;
}

/**
 * Get the most relevant mealtime at current time
 *
 * @return ICE\ERP\HRM\Mealtime
 */
function get_current_mealtime() {
    $mealtimes = get_mealtimes();
    $first = null;
    $cur_obj = null;
    foreach ( $mealtimes as $meal => $meal_obj ) {
        if ( $meal_obj->is_open() ) {
            return $meal_obj;
        } elseif ( $meal_obj->is_pending() ) {
            if ( null === $cur_obj || -1 === compare_mealtimes( $meal_obj, $cur_obj ) ) {
                $cur_obj = $meal_obj;
            }
        }
        if ( null === $first ) {
            $first = $meal_obj;
        }
    }
    if ( null === $cur_obj ) {
        $cur_obj = $first;
    }
    return $cur_obj;
}
