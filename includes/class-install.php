<?php
/**
 * Install plugin food
 *
 * @package  Werp_hr_food
 * @author   Talha Ahmed <talha.ahmed@gmail.com>
 * @license  Unlicensed http://www.iceanimations.com
 * @link     http://www.iceanimations.com
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

require_once WPERP_FOOD_LIBS . '/class-tgm-plugin-activation.php';

/**
 * Class Install
 *
 * @package  erp_hr_food
 * @author   Talha Ahmed <talha.ahmed@gmail.com>
 * @license  Unlicensed http://www.iceanimations.com
 * @link     http://www.iceanimations.com
 */
class Install {

    /**
     * Install constructor
     *
     * @return null
     */
    public function __construct() {
        if ( (defined( 'DOING_AJAX' ) && DOING_AJAX)
            || (defined( 'DOING_CRON' ) && DOING_CRON)
        ) {
            return;
        }
        add_action( 'tgmpa_register', [ $this, 'register_require_plugins' ] );
        register_activation_hook( WPERP_FOOD_FILE, array( $this, 'activate' ) );
        register_deactivation_hook( WPERP_FOOD_FILE, array( $this, 'deactivate' ) );
        $this->create_table();
    }

    /**
     * Function runs on the plugin activation hook
     *
     * @return void
     */
    public function activate() {
        if ( version_compare( get_bloginfo( 'version' ), '4.4', '<' ) ) {
            wp_die( esc_html_e( 'You must upgrade WordPress to use this plugin', 'erp-food' ) );
        }
    }

    /**
     * Get Collation from mysql database
     *
     * @return string
     */
    private function get_collation() {
        global $wpdb;

        $collate = '';
        if ( $wpdb->has_cap( 'collation' ) ) {
            if ( ! empty( $wpdb->charset ) ) {
                $collate .= "DEFAULT CHARACTER SET $wpdb->charset";
            }

            if ( ! empty( $wpdb->collate ) ) {
                $collate .= " COLLATE $wpdb->collate";
            }
        }
        return $collate;
    }

    /**
     * Check for required plugins in the current WordPress installation
     *
     * @return void
     */
    public function register_require_plugins() {

        $plugins = array(
            array(
                'name'     => 'WP ERP',
                'slug'     => 'erp',
                'required' => true,
            ),
            array(
                'name'     => 'WP ERP - Attendance Management',
                'slug'     => 'erp-attendance',
                'required' => true,
                'source'   => 'https://bitbucket.org/iceanimations/erp-attendance',
            ),
        );

        $config = array(
            'id'           => 'erp-food',
            'default_path' => '',
            'menu'         => 'tgmpa-install-plugins',
            'parent_slug'  => 'plugins.php',
            'capability'   => 'manage_options',
            'has_notices'  => true,
            'dismissable'  => true,
            'is_automatic' => true,
        );

        tgmpa( $plugins, $config );
    }

    /**
     * Create table erp_hr_food_order
     *
     * @return void
     */
    public function create_table() {
        global $wpdb;

        $collate = $this->get_collation();

        $table_schema = [
            "CREATE TABLE IF NOT EXISTS {$wpdb->prefix}erp_hr_food_order(
              `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
              `user_id` bigint(20) unsigned NOT NULL,
              `date` date NOT NULL,
              `food_id` bigint(20) unsigned NOT NULL,
              `rating` float(3, 1) unsigned,
              `mealtime` varchar(20) NOT NULL,
              `status` varchar(20) NOT NULL default 'pending',
              `comment_id` bigint(20) unsigned DEFAULT NULL,
              `updated_at` datetime NOT NULL,
              `created_at` datetime NOT NULL,
              `deleted_at` datetime DEFAULT NULL,
              KEY `id` (`id`)
            ) $collate;"
        ];

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        foreach ( $table_schema as $table ) {
            dbDelta( $table );
        }
    }

    /**
     * Clean up at plugin deactivation
     */
    public function deactivate() {
        global $wpdb;
        $drop = 'DROP TABLE IF EXISTS ' . $wpdb->prefix . 'erp_hr_food_order;';
        $wpdb->query( $drop );
    }
}


new Install();
