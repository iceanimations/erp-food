<?php
/**
 * This file contains the mealtime class
 *
 * @package erp_hr_food
 * @author Talha Ahmed <talha.ahmed@gmail.com>
 */

namespace ICE\ERP\HRM;

/**
 * Class: Mealtime
 */
class Mealtime {
    /**
     * Keys for settings and defaults
     *
     * @var string
     */
    public static $keys = [ 'open_time', 'close_time', 'enabled', 'criterion' ];

    /**
     * ID or slug of the mealtime
     *
     * @var string
     */
    private $title = '';

    /**
     * ID or slug of the mealtime
     *
     * @var string
     */
    private $id = '';

    /**
     * Array of default settings ... must define all the '$keys'
     *
     * @var array
     */
    private $defaults = [];

    /**
     * Constructor for Mealtime
     *
     * @param string $id The slug of id of the mealtime.
     * @param string $title The title.
     * @param array  $defaults Array of the defaults must have all the keys.
     *
     * @throws \Exception If $defaults does not have all the keys.
     */
    function __construct( $id, $title, $defaults ) {
        $this->id = $id;
        $this->title = $title;
        foreach ( Mealtime::$keys as $key ) {
            if ( ! isset( $defaults[$key] ) ) {
                throw new \Exception( "Must set key {$key} on defaults array" );
            }
        }
        $this->defaults = $defaults;
    }

    /**
     * Get id.
     *
     * @return id
     */
    public function get_id() {
        return $this->id;
    }

    /**
     * Get defaults.
     *
     * @return defaults
     */
    public function get_defaults() {
        return $this->defaults;
    }

    /**
     * Get title.
     *
     * @return string title
     */
    public function get_title() {
        return $this->title;
    }

    /**
     * Get the default value for the given key.
     *
     * @param string $key Key for which value must be fetched.
     */
    public function get_default( $key ) {
        return $this->defaults[$keys];
    }

    /**
     * Get all settings for this meal time
     */
    public function get_settings() {
        $settings = array();
        foreach ( Mealtime::$keys as $key ) {
            $settings[$key] = $this->get( $key );
        }
        return $settings;
    }

    /**
     * Get the value of key from the settings or defaults if not there.
     *
     * @param string $key Key to get.
     */
    public function get( $key ) {
        if ( 'title' === $key ) {
            return $this->get_title();
        }
        if ( 'id' === $key ) {
            return $this->get_id();
        }
        return get_option( $this->id . '_' . $key, $this->defaults[$key] );
    }

    /**
     * Check if the meal time is open now
     *
     * @return bool
     */
    public function is_open() {
        if ( ! $this->is_enabled() ) {
            return false;
        }
        $open_time = strtotime( $this->get( 'open_time' ) );
        $close_time = strtotime( $this->get( 'close_time' ) );
        $now = strtotime( current_time( 'H:i' ) );
        return $now >= $open_time && $now <= $close_time;
    }

    /**
     * Check if mealtime will open later or not
     *
     * @return bool
     */
    public function is_pending() {
        if ( ! $this->is_enabled() ) {
            return false;
        }
        $now = strtotime( current_time( 'H:i' ) );
        $open_time = strtotime( $this->get( 'open_time' ) );
        return $now < $open_time;
    }

    /**
     * Check if the entry time (no dates) is eligible
     *
     * @param mixed  $entrytime Time to check with.
     * @param string $type Whether 'timestamp' or 'mysql'.
     *
     * @return bool
     */
    public function is_eligible( $entrytime, $type = 'timestamp' ) {
        if ( ! $this->is_enabled() ) {
            return false;
        }
        if ( 'timestamp' !== $type ) {
            $entrytime = strtotime( $entrytime );
        }
        if ( strtotime( $this->get( 'criterion' ) ) >= $entrytime ) {
            return true;
        }
        return false;
    }

    /**
     * Check if the mealtime is enabled
     */
    public function is_enabled() {
        return 'yes' === $this->get( 'enabled' );
    }

    /**
     * Convert this object to an array.
     *
     * @return array
     */
    public function as_array() {
        return array(
            'id'    => $this->id,
            'title' => $this->title,
            'defaults' => $this->get_defaults(),
            'settings' => $this->get_settings()
        );
    }

    /**
     * Get all the foods available at this meal time.
     *
     * @param string $post_type the food post_type.
     *
     * @return menu
     */
    public function get_menu( $post_type ) {
        $query = new \WP_Query( array( 'post_type' => $post_type ) );
        $menu = array();
        $meta_key = $post_type . '_data';
        foreach ( $query->posts as $post ) {
            $food = array();
            $food['post'] = $post;
            $meta = get_post_meta( $post->ID, $meta_key, true );
            $has_meal = in_array( $this->id, explode( ',', $meta[ 'mealtime' ] ) );
            $available = 'yes' === $meta[ 'available' ];
            $food[ 'meta_data' ] = $meta;
            if ( $has_meal && $available ) {
                array_push( $menu, $food );
            }
        }
        return $menu;
    }

    /**
     * Get a time difference expression for the current mealtime in a human
     * readable way
     */
    public function get_time_diff_desc() {
        $desc = '';
        $time = current_time( 'H:i' );
        $now = strtotime( $time );
        $close_time = strtotime( $this->get( 'close_time' ) );
        $open_time = strtotime( $this->get( 'open_time' ) );
        $desc = '@' . $time . ' ';
        if ( $this->is_open() ) {
            $desc = $desc . 'There are ' . human_time_diff( $now, $close_time ) . ' until ' . $this->get_title() . ' closes';
        } elseif ( $this->is_pending() ) {
            $desc = $desc . 'There are ' . human_time_diff( $open_time, $now ) . ' until ' . $this->get_title() . ' opens';
        } else {
            $desc = $desc . 'There have been ' . human_time_diff( $close_time, $now ) . ' since ' . $this->get_title() . ' closed';
        }
        return $desc;
    }


    /**
     * A string to describe the current mealtime
     */
    function get_desc() {
        if ( $this->is_open() ) {
            return $this->get_title() . ' is open now';
        } elseif ( $this->is_pending() ) {
            return $this->get_title() . ' is pending';
        } else {
            return $this->get_title() . ' is now closed';
        }
    }

}
