<?php
/**
 * File containing food class
 *
 * @package erp_hr_food
 * @author Talha Ahmed
 */

namespace ICE\ERP\HRM;

/**
 * Food class HR
 *
 * Food for employees who do overtime
 *
 * @author Talha Ahmed <talha.ahmed@iceanimations.net>
 */
class Food {

    /**
     * Post type of the food used to register stuff
     *
     * @var string
     */
    private $post_type = 'erp_hr_food';

    /**
     * List name and defaults of mealtimes
     *
     * @var mixed
     */
    private $meals = [];

    /**
     * Constructor for Food Class
     */
    function __construct() {
        $this->meals = get_mealtimes();
    }

    /**
     * Get post_type.
     *
     * @return post_type.
     */
    public function get_post_type() {
        return $this->post_type;
    }

    /**
     * Init all actions
     *
     * @return void
     */
    public function init_actions() {
        $sp_action = 'save_post_' . $this->post_type;

        add_action( 'init', array( $this, 'register_post_type'  ) );
        add_action( 'admin_init', array($this, 'admin_init') );
        add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );
        add_action( 'save_post_' . $this->post_type, array( $this, 'save_post' ), 10, 3 );
        add_action( 'wp_ajax_erp_hr_food_order_rate', array( $this, 'rate_order' ) );
        add_action( 'wp_ajax_erp_hr_food_order_place', array( $this, 'place_order' ) );
        add_action( 'wp_ajax_erp_hr_food_order_cancel', array( $this, 'cancel_order' ) );
        add_action( 'erp_hr_employee_single_tabs', array( $this, 'employee_single_tabs' ) );
        add_action( "manage_{$this->post_type}_posts_columns", array( $this, 'manage_columns' ) );
        add_action( "manage_{$this->post_type}_posts_custom_column", array( $this, 'manage_custom_column' ), 10, 2 );
    }

    /**
     * Register all filter hooks here.
     */
    public function init_filters() {
        add_filter( 'the_content', array( $this, 'filter_content' ) );
        add_filter( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ), 9999 );
        add_filter( 'erp_settings_hr_sections', [ $this, 'add_food_sections' ] );
        add_filter( 'erp_settings_hr_section_fields', [ $this, 'add_food_section_fields' ], 10, 2 );
    }

    /**
     * Register Food Post Type
     */
    public function register_post_type() {
        $capability = 'erp_hr_manager';
        $labels = array(
            'name'               => __( 'Foods', 'erp-food' ),
            'singular_name'      => __( 'Food', 'erp-food' ),
            'menu_name'          => __( 'Office Food Menu', 'erp-food' ),
            'name_admin_bar'     => __( 'Office Food Menu', 'erp-food' ),
            'add_new'            => __( 'Add New', 'erp-food' ),
            'add_new_item'       => __( 'Add New Food', 'erp-food' ),
            'new_item'           => __( 'New Food', 'erp-food' ),
            'edit_item'          => __( 'Edit Food', 'erp-food' ),
            'view_item'          => __( 'View Food', 'erp-food' ),
            'all_items'          => __( 'All Foods', 'erp-food' ),
            'search_items'       => __( 'Search Foods', 'erp-food' ),
            'parent_item_colon'  => __( 'Parent Foods:', 'erp-food' ),
            'not_found'          => __( 'No foods found.', 'erp-food' ),
            'not_found_in_trash' => __( 'No foods found in Trash.', 'erp-food' ),
        );
        $args = array(
            'labels'              => $labels,
            'description'         => __( 'Food available at the time of dinner / lunch', 'erp-food' ),
            'public'              => true,
            'publicly_queryable'  => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'query_var'           => true,
            'exclude_from_search' => true,
            'rewrite'             => array( 'slug' => 'erp-hr-food' ),
            'capabilities'        => array(
                'edit_post'          => $capability,
                'read_post'          => $capability,
                'delete_posts'       => $capability,
                'edit_posts'         => $capability,
                'edit_others_posts'  => $capability,
                'publish_posts'      => $capability,
                'read_private_posts' => $capability,
                'create_posts'       => $capability,
                'delete_post'        => $capability,
            ),
            'capability_type'     => 'post',
            'has_archive'         => true,
            'hierarchical'        => false,
            'menu_icon'           => 'dashicons-editor-kitchensink',
            'menu_position'       => null,
            'supports'            => array( 'title', 'editor', 'thumbnail', 'comments' ),
            'taxonomies'          => array( 'post_tag' )
        );
        register_post_type( $this->post_type, $args );
    }

    /**
     * Add action to add_meta_box hook
     */
    public function admin_init() {
        add_action( 'add_meta_boxes_' . $this->post_type, array( $this, 'create_meta_boxes' ) );
    }

    /**
     * Create Food meta boxes
     */
    public function create_meta_boxes() {
        add_meta_box(
            $this->post_type . '_mb',
            __( 'Food Info', 'erp-food' ),
            array($this, 'create_food_options_metabox'),
            $this->post_type,
            'side',
            'high'
        );
    }

    /**
     * Creating the actual metabox for food
     *
     * @param mixed $post Post Data as passed by WordPress.
     * @return void
     */
    public function create_food_options_metabox( $post ) {
        include WPERP_FOOD_VIEWS . '/food-options-metabox.php';
    }

    /**
     * Add custom columns to food post type.
     *
     * @param mixed $columns List of already created columns.
     */
    public function manage_columns( $columns ) {
        $new_columns = array();
        $new_columns['cb'] = $columns['cb'];
        unset( $columns['cb'] );
        $new_columns['title'] = $columns['title'];
        unset( $columns['title'] );
        $new_columns['mealtime'] = __( 'Meal times', 'erp-food' );
        $new_columns['available'] = __( 'Available', 'erp-food' );
        $new_columns['vendor'] = __( 'Vendor', 'erp-food' );
        $new_columns['helpings'] = __( 'Helpings', 'erp-food' );
        return array_merge( $new_columns, $columns );
    }

    /**
     * Add content in food list columns
     *
     * @param mixed $column The column to manage.
     * @param mixed $post_id The post id.
     */
    public function manage_custom_column( $column, $post_id ) {
        $data = get_post_meta( $post_id, $this->post_type . '_data', true );
        switch ( $column ) {
        case 'mealtime':
            echo esc_html( $data['mealtime'] );
            break;
        case 'available':
            echo esc_html( $data['available'] );
            break;
        case 'vendor' :
            echo esc_html( $data['vendor'] );
            break;
        case 'helpings':
            echo esc_html( $data['helpings'] );
            break;
        default:
            break;
        }
    }

    /**
     * Enqueue scripts in the admin interface
     */
    public function admin_enqueue_scripts() {
        global $typenow;

        //if ( $typenow !== $this->post_type ) {
            //return;
        //}

        $bootstrap_css = WPERP_FOOD_CSS_LIBS . '/bootstrap.min.css';
        wp_register_style( 'erp_hr_food_bootstrap', $bootstrap_css );
        wp_enqueue_style( 'erp_hr_food_bootstrap' );

        $bootstrap_js = WPERP_FOOD_JS_LIBS . '/bootstrap.min.js';
        wp_register_script( 'erp_hr_food_bootstrap', $bootstrap_js, true );
        wp_enqueue_script( 'erp_hr_food_bootstrap' );

        $rateit_css = WPERP_FOOD_CSS_LIBS . '/rateit.min.css';
        wp_register_style( 'erp_hr_food_rateit', $rateit_css, array(), '4.7.4', false );
        wp_enqueue_style( 'erp_hr_food_rateit' );

        $rateit_js = WPERP_FOOD_JS_LIBS . '/jquery.rateit.min.js';
        wp_register_script( 'erp_hr_food_rateit', $rateit_js, array(), '4.7.4', true );
        wp_enqueue_script( 'erp_hr_food_rateit' );

        $food_main_js = WPERP_FOOD_ASSETS . '/js/food.js';
        wp_register_script( 'erp_hr_food_food', $food_main_js, array(), '1.0.0', true );
        wp_enqueue_script( 'erp_hr_food_food' );

        wp_localize_script( 'erp_hr_food_food', 'food_obj', array(
            'ajax_url' => admin_url( 'admin-ajax.php' )
        ) );
    }

    /**
     * Save Food Entry
     *
     * @param int    $post_id ID of the post.
     * @param string $post    The post itself.
     * @param bool   $update  Indicate whether it is an update on an old post.
     */
    public function save_post( $post_id, $post, $update ) {
        if ( ! $update ) {
            return;
        }

        $post = filter_input_array( INPUT_POST );

        $mealtime = array();
        foreach ( $this->meals as $meal => $defaults ) {
            if ( 'yes' === $post[ 'food_' . $meal ] ) {
                array_push( $mealtime, $meal );
            }
        }
        $mealtime = join( ',', $mealtime );

        $food_data = $array();
        if ( $update ) {
            $food_data = get_post_meta( $post_id, $this->post_type . '_data', true );
        }

        $food_data = array_merge( $food_data, array (
            'mealtime'     => sanitize_text_field( $mealtime ),
            'helpings'     => sanitize_text_field( $post['food_helpings'] ),
            'vendor'       => sanitize_text_field( $post['food_vendor'] ),
            'available'    => sanitize_text_field( $post['food_available'] ),
        ) );

        if ( ! $update ) {
            $food_data = array_merge( $food_data, array(
                'rating'       => 0,
                'rating_count' => 0,
                'orders'       => 0
            ) );
        }

        update_post_meta( $post_id, $this->post_type . '_data', $food_data );
    }

    /**
     * Filter Food Content
     *
     * @param string $content content of post send by WordPress.
     * @return string $content
     */
    public function filter_content( $content ) {
        if ( ! is_singular( $this->post_type ) ) {
            return $content;
        }
        global $post;
        $food_data = get_post_meta( $post->ID, $this->post_type . '_data', true );
        $food_template_res = wp_remote_get( WPERP_FOOD_URL . '/views/food-template.php' );
        $food_html = wp_remote_retrieve_body( $food_template_res );
        $food_html = str_replace( 'MEAL_TIME_PH'   , $food_data['mealtime']       , $food_html );
        $food_html = str_replace( 'HELPINGS_PH'    , $food_data['helpings']       , $food_html );
        $food_html = str_replace( 'VENDOR_PH'      , $food_data['vendor']         , $food_html );
        $food_html = str_replace( 'AVAILABLE_PH'   , $food_data['available']      , $food_html );
        $food_html = str_replace( 'ERP_HR_FOOD_ID' , $post->ID                    , $food_html );
        $food_html = str_replace( 'ORDERCNT_PH'    , $food_data['orders']         , $food_html );
        $food_html = str_replace( 'RATINGCNT_PH'   , $food_data['rating_count']   , $food_html );
        $food_html = str_replace( 'FOOD_RATING'    , $food_data['rating']         , $food_html );
        $food_html = str_replace( 'READONLY_PH'    , 'true'                       , $food_html );

        $food_html = str_replace( 'MEALTIME_I18N'  , esc_html__( 'MEAL TIME '   , 'erp-food' ) , $food_html );
        $food_html = str_replace( 'HELPINGS_I18N'  , esc_html__( 'HELPINGS '    , 'erp-food' ) , $food_html );
        $food_html = str_replace( 'VENDOR_I18N'    , esc_html__( 'VENDOR '      , 'erp-food' ) , $food_html );
        $food_html = str_replace( 'AVAILABLE_I18N' , esc_html__( 'AVAILABLE '   , 'erp-food' ) , $food_html );
        $food_html = str_replace( 'RATING_I18N'    , esc_html__( 'RATING '      , 'erp-food' ) , $food_html );
        $food_html = str_replace( 'ORDERCNT_I18N'  , esc_html__( 'ORDER COUNT ' , 'erp-food' ) , $food_html );
        $food_html = str_replace( 'RATINGCNT_I18N' , esc_html__( ' ratings '      , 'erp-food' ) , $food_html );
        return $content . $food_html;
    }

    /**
     * Link all CS and JS scripts
     *
     * @return void
     */
    public function enqueue_scripts() {
        $bootstrap = WPERP_FOOD_CSS_LIBS . '/bootstrap.min.css';
        wp_register_style( 'erp_hr_food_bootstrap', $bootstrap );
        wp_enqueue_style( 'erp_hr_food_bootstrap' );

        $rateit_css = WPERP_FOOD_CSS_LIBS . '/rateit.min.css';
        wp_register_style( 'erp_hr_food_rateit', $rateit_css, array(), '4.7.4', false );
        wp_enqueue_style( 'erp_hr_food_rateit' );

        $rateit_js = WPERP_FOOD_JS_LIBS . '/jquery.rateit.min.js';
        wp_register_script( 'erp_hr_food_rateit', $rateit_js, array(), '4.7.4', true );
        wp_enqueue_script( 'erp_hr_food_rateit' );

        $food_main_js = WPERP_FOOD_ASSETS . '/js/food.js';
        wp_register_script( 'erp_hr_food_food', $food_main_js, array(), '1.0.0', true );
        wp_enqueue_script( 'erp_hr_food_food' );

        wp_localize_script( 'erp_hr_food_food', 'food_obj', array(
            'ajax_url' => admin_url( 'admin-ajax.php' )
        ) );
    }

    /**
     * Recieve ajax request for food rating
     *
     * @return void
     */
    public function rate_order() {

        $post = filter_input_array( INPUT_POST );
        $food_order_id = $post['food_order_id'];
        $rating = $post['rating'];
        $comment = $post['comment'];

        $order = Models\Food_Order::find( $food_order_id );
        if ( null === $order ) {
            wp_send_json( array(
                'response' => false,
                'message' => 'bad food id'
            ) );
        }

        $current_user = wp_get_current_user();
        $current_user_id = $current_user->ID;
        if ( (int) $order->user_id !== (int) $current_user_id ) {
            wp_send_json( array(
                'response' => false,
                'message' => 'not current user' . $order->user_id . ' ' . $current_user->ID
            ) );
        }

        $comment_id = $order->comment_id;
        if ( null === $comment_id && $comment ) {
            $comment_id = wp_new_comment( array(
                'comment_post_ID' => $order->food_id,
                'comment_content' => $post['comment'],
                'comment_approved' => 0,
                'user_id' => $current_user_id,
            ) );
            add_comment_meta( $comment_id, 'erp_hr_food_order_id', $food_order_id );
        } elseif ( $comment ) {
            wp_update_comment( array(
                'comment_ID' => $comment_id,
                'comment_approved' => 0,
                'comment_content' => $comment
            ) );
            update_comment_meta( $comment_id, 'erp_hr_food_order_id', $food_order_id );
        }

        $order->comment_id = $comment_id;
        $order->rating = (float) $rating;
        $order->save();

        $this->update_counts( $order->food_id );

        wp_send_json( array(
            'response' => true, 'message' => 'rating saved!',
            'rating' => $rating, 'comment' => $comment
        ) );
    }

    /**
     * Handle cancel order ajax call
     */
    public function cancel_order() {
        $post = filter_input_array( INPUT_POST );

        $date = $post['date'];
        if ( current_time( 'Y-m-d' ) !== $date ) {
            wp_send_json(
                array(
                    'response' => false,
                    'message' => 'Order Outdated: Try Refreshing before
                    submitting again' . $current_time( 'Y-m-d' ) . ' ' . $date
                )
            );
            return;
        }

        $meal = get_mealtimes()[$post['mealtime']];
        if ( ! $meal->is_open( $this->post_type ) ) {
            wp_send_json(
                array (
                    'response' => false,
                    'message'  => 'Mealtime ' . $meal-> get_title() . ' is not open'
                )
            );
            return;
        }

        $current_order = Models\Food_Order::where( 'user_id', $post['user_id'] )
                ->where( 'date', $date )
                ->where( 'mealtime', $meal->get_id() )
                ->first();

        if ( null === $current_order ) {
            wp_send_json(
                array (
                    'response' => false,
                    'message'  => 'No such order found, Try refreshing the page!'
                )
            );
            return;
        }

        $old_food_id = $current_order->food_id;

        $current_order->delete();

        $this->update_counts( $old_food_id );

        wp_send_json(
            array (
                'response' => true,
                'message'  => 'Order cancelled successfully'
            )
        );
    }

    /**
     * Handle AJAX action place_order
     */
    public function place_order() {
        $post = filter_input_array( INPUT_POST );

        $date = $post['date'];
        if ( current_time( 'Y-m-d' ) !== $date ) {
            wp_send_json(
                array(
                    'response' => false,
                    'message' => 'Order Outdated: Try Refreshing before submitting again' . $current_time( 'Y-m-d' ) . ' ' . $date
                )
            );
            return;
        }

        $meal = get_mealtimes()[$post['mealtime']];
        if ( ! $meal->is_open( $this->post_type ) ) {
            wp_send_json(
                array (
                    'response' => false,
                    'message'  => 'Mealtime ' . $meal-> get_title() . ' is not open'
                )
            );
            return;
        }

        $user_id = $post['user_id'];
        $emp = \WeDevs\ERP\HRM\Models\EmployeeExtended::where( 'user_id', $user_id )->first();
        if ( null === $emp ) {
            wp_send_json(
                array (
                    'response' => false,
                    'message'  => 'User is not an employee'
                )
            );
            return;
        }

        $food_id = (int) $post['food_id'];
        $food_valid = false;

        foreach ( $meal->get_menu( $this->post_type ) as $food ) {
            if ( $food['post']->ID === (int) $food_id ) {
                $food_valid = true;
            }
        }

        if ( ! $food_valid ) {
            wp_send_json(
                array (
                    'response' => false,
                    'message'  => 'Food is not available ... Refresh page and Try again',
                )
            );
            return;
        }

        $current_order = Models\Food_Order::firstOrCreate(
            array(
                'user_id' => $user_id,
                'date'    => $date,
                'mealtime' => $meal->get_id(),
            )
        );

        $old_food_id = $current_order->food_id;

        $current_order->food_id = $food_id;
        $current_order->save();

        if ( $old_food_id ) {
            $this->update_counts( $old_food_id );
        }

        $this->update_counts( $food_id );

        wp_send_json(
            array (
                'response' => true,
                'message'  => 'Order submitted successfully'
            )
        );
    }

    /**
     * Update counts in food meta data
     *
     * @param mixed $food_id Post id for the food post whose meta is to be
     * modified.
     */
    public function update_counts( $food_id ) {
        $post = get_post( $food_id );
        if ( $this->post_type !== $post->post_type ) {
            return;
        }
        $orders = Models\Food_Order::where( 'food_id', (int) $food_id );
        $num_orders = $orders->count();
        $ratings = $orders->whereNotNull( 'rating' );
        $num_ratings = $ratings->count();
        $avg_rating = $ratings->avg( 'rating' );

        $post_meta = get_post_meta( $food_id, $this->post_type . '_data', true );
        $post_meta['rating'] = $avg_rating;
        $post_meta['rating_count'] = $num_ratings;
        $post_meta['orders'] = $num_orders;

        update_post_meta( $food_id, $this->post_type . '_data', $post_meta );
    }

    /**
     * Add Food Sections to ERP HR Settings Page
     *
     * @param array $sections Already existing sections.
     * @return array
     */
    public function add_food_sections( $sections ) {

        $sections ['food'] = __( 'Food Settings', 'erp-food' );
        return $sections;
    }

    /**
     * Add fields to Attendance Section in ERP Fields
     *
     * @param array  $fields existing fields.
     * @param fields $section name of the section.
     *
     * @return array
     */
    public function add_food_section_fields( $fields, $section ) {

        if ( 'food' === $section ) {

            $section_fields = array();

            foreach ( $this->meals as $meal => $obj ) {
                $defaults = $obj->get_defaults();
                array_push( $section_fields,
                    // Mealtime Settings.
                    array(
                        'title' => $obj->get_title() . __( ' Settings', 'erp-food' ),
                        'type'  => 'title',
                        'id'    => $obj->get_id() . '_title'
                    ),
                    array(
                        'title' => $obj->get_title() . __( ' Enable Orders', 'erp-food' ),
                        'type'  => 'checkbox',
                        'id'    => $obj->get_id() . '_enabled',
                        'default' => $defaults['enabled']
                    ),
                    array(
                        'title'   => $obj->get_title() . __( ' Eligibility', 'erp-food' ),
                        'type'    => 'text',
                        'id'      => $obj->get_id() . '_criterion',
                        'desc'    => __( 'If an employee comes after this time he will not eligible to order this meal', 'erp-food' ),
                        'class'   => 'attendance-time-field',
                        'default' => $defaults['criterion']
                    ),
                    array(
                        'title'   => $obj->get_title() . __( ' Open Time', 'erp-food' ),
                        'type'    => 'text',
                        'id'      => $obj->get_id() . '_open_time',
                        'desc'    => __( 'An order for this meal time cannot be placed before this time', 'erp-food' ),
                        'class'   => 'attendance-time-field',
                        'default' => $defaults['open_time']
                    ),
                    array(
                        'title'   => $obj->get_title() . __( ' Close Time', 'erp-food' ),
                        'type'    => 'text',
                        'id'      => $obj->get_id() . '_close_time',
                        'desc'    => __( 'An order for this meal time cannot be placed after this time', 'erp-food' ),
                        'class'   => 'attendance-time-field',
                        'default' => $defaults['close_time']
                    ),
                    array(
                        'type'  => 'sectionend',
                        'id'    => 'script_styling_options'
                    )
                );
            }; // End foreach().
            $fields[$section] = $section_fields;
        } // End if().
        return $fields;
    }

    /**
     * HR Single Employee Callback
     *
     * @param array $tabs Array of already existing tabs.
     */
    public function employee_single_tabs( $tabs ) {
        $tabs[ 'food' ] = array(
            'title'     => __( 'Food Ordering', 'erp-food' ),
            'callback'  => array( $this, 'single_food_ordering_tab' )
        );
        return $tabs;
    }

    /**
     * Create a food orders tab for a single employee.
     */
    public function single_food_ordering_tab() {
        include WPERP_FOOD_INCLUDES . '/class-food-orders.php';
    }

    /**
     * Get all posts of food type
     */
    public function get_all() {
        $query = new WP_Query( array( 'post_type' => $this->post_type ) );
        return $query->posts();
    }

} // END class.
