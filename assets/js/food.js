
function pause(milliseconds) {
	var dt = new Date();
	while ((new Date()) - dt <= milliseconds) { /* Do nothing */ }
}


jQuery(function($) {
    $('.erp_hr_food_order_submit_rating').click(function() {
        var myid = $(this).data('food_order_id');
        var rateit = $('#erp_hr_food_order_rating_new_' + myid);
        var comment = $('#erp_hr_food_order_comment_new_' + myid);

        var formObj = {
            action: "erp_hr_food_order_rate",
            food_order_id: myid,
            rating: rateit.rateit("value"),
            comment: comment.val(),
            //XDEBUG_SESSION_START: true
        };

        $.post( food_obj.ajax_url, formObj, function(data) {
            //console.log( data );
            if ( data.response === true ) {
                $('#erp_hr_food_order_rating_' + myid).rateit("value", rateit.rateit("value"));
                $('#erp_hr_food_order_comment_' + myid).html(comment.val());
            }
        } );
    } );


    var update_response = function( data ) {
        //console.log( data );
        if ( data.response === true ) {
            $('#erp_hr_food_submission_result').prop('class', 'well text-success');
        }
        else {
            $('#erp_hr_food_submission_result').prop('class', 'well text-danger');
            $('#erp_hr_food_place_order').prop('disabled', false);
            $('#erp_hr_food_cancel_order').prop('disabled', false);
        }
        $('#erp_hr_food_submission_result').html(data.message);
    };

    var place_order = function () {
        var formObj = {
            action: "erp_hr_food_order_place",
            user_id: $(this).attr("user_id"),
            food_id: $('input:radio[name=erp_hr_food_id]:checked').val(),
            mealtime: $(this).attr("mealtime"),
            date: $(this).attr("date"),
            //XDEBUG_SESSION_START: true
        };

        //console.log( formObj );

        $('#erp_hr_food_place_order').prop('disabled', true);
        $('#erp_hr_food_cancel_order').prop('disabled', true);
        $('#erp_hr_food_submission_result').prop('class', 'well text-info');
        $('#erp_hr_food_submission_result').html('Submitting food order ...');

        $.post( food_obj.ajax_url, formObj, update_response );

    };

    var cancel_order = function () {
        var formObj = {
            action: "erp_hr_food_order_cancel",
            user_id: $(this).attr("user_id"),
            mealtime: $(this).attr("mealtime"),
            date: $(this).attr("date"),
            //XDEBUG_SESSION_START: true
        };

        $('#erp_hr_food_place_order').prop('disabled', true);
        $('#erp_hr_food_cancel_order').prop('disabled', true);
        $('#erp_hr_food_submission_result').prop('class', 'well text-info');
        $('#erp_hr_food_submission_result').html('Canceling food order ...');

        $.post( food_obj.ajax_url, formObj, update_response );
    };

    $('#erp_hr_food_place_order').click( place_order );
    $('#erp_hr_food_cancel_order').click( cancel_order );

});
