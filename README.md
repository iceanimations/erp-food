# [WPERP Office Food](https://www.iceanimations.com)

## Features
* Ensure dependency plugins
* Add / Edit Food Entries as post type
* Manage custom metadata such as mealtimes, availability and ratings.
* Edit settings for several Mealtimes
* Manage Taking of orders for people who fulfill the criterion.

## Requirementsj
### Non Dev
* Wordpress
* WPERP
* WPERP Attendance Plugin
* PHP >= 5.6
### Dev
* Composer - [Install](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx)
* nodejs, npm, gulp-cli

## Installation
### Dev
* Use gulp build to generate build zip
### Non Dev
* unpack zip into the wordpress plugins folder

## Deploys
Will be added later

## Documentation
Sorry! None Yet

## Contributing
This is a private project. Copyright
[ICE Animations (Pvt.) Ltd.](http://www.iceanimations.com) 2017

## License
Unlicensed - No one is allowed to use, possess, deploy or modify this code
except with explicit permission and authority of ICE Animations Pvt. Ltd.
Pakistan.
