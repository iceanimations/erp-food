//modules
var gulp       = require('gulp');
var del        = require('del');
var zip        = require('gulp-zip');
var livereload = require('gulp-livereload');
var fs         = require('fs');
var path       = require('path');
var bowerMain  = require('bower-main');
var less       = require('gulp-less');
var cleanCSS   = require('gulp-clean-css');
var cleanJS    = require('gulp-uglify');
var ext        = require('gulp-ext-replace');

// determine package version
var pkg = JSON.parse(fs.readFileSync('./package.json').toString());

// folders that are part of the package
var publishFolder = {
    css             : 'assets/css/*.css',
    js              : 'assets/js/*.js',
    css_libs        : 'assets/css/libs/*.css',
    js_libs         : 'assets/js/libs/*.js',
    css_lib_gifs    : 'assets/css/libs/*.gif',
    models          : 'includes/model/*.php',
    updates         : 'includes/updates/*.php',
    includes        : 'includes/*.php',
    libs            : 'includes/libs/*.php',
    languages       : 'languages/*.pot',
    views           : 'views/*.php',
    main            : 'erp-food.php',
    uninstall       : 'uninstall.php',
    changelog       : 'CHANGELOG.md'
};

 //deploy and release folders
var pluginBase =  './web/app/plugins/';
var buildBase =  './build/';
var pluginFolder = pluginBase+pkg.name+'/';
var buildFolder = buildBase+pkg.name+'/';

var devcopy_tasks = [];
var build_tasks = [];

// populate assets with bower
gulp.task('assets', function() {

    var bower_js = bowerMain('js', 'min.js');
    var bower_css  = bowerMain('css', 'min.css');
    var bower_less = bowerMain('less');
    var bower_gif = bowerMain('gif');

    gulp.src('bower_components/jquery.rateit/scripts/*.gif').
        pipe(gulp.dest(path.dirname(publishFolder.css_lib_gifs)));

    gulp.src(bower_js.normal).
        pipe(gulp.dest(path.dirname(publishFolder.js_libs)));
    gulp.src(bower_js.minified).
        pipe(gulp.dest(path.dirname(publishFolder.js_libs)));
    gulp.src(bower_js.minifiedNotFound).
        pipe(cleanJS()).
        pipe(ext('min.js')).
        pipe(gulp.dest(path.dirname(publishFolder.js_libs)));

    gulp.src(bower_css.minifiedNotFound).
        pipe(less()).
        pipe(cleanCSS()).
        pipe(ext('min.css')).
        pipe(gulp.dest(path.dirname(publishFolder.css_libs)));
    gulp.src(bower_less.normal).
        pipe(less()).
        pipe(gulp.dest(path.dirname(publishFolder.css_libs))).
        pipe(cleanCSS()).
        pipe(ext('min.css')).
        pipe(gulp.dest(path.dirname(publishFolder.css_libs)));
    gulp.src(bower_css.normal).
        pipe(gulp.dest(path.dirname(publishFolder.css_libs)));
    gulp.src(bower_css.minified).
        pipe(gulp.dest(path.dirname(publishFolder.css_libs)));
});

gulp.task('clean:assets', function() {
    del([publishFolder.css_libs, publishFolder.js_libs]);
});

function create_copy_task_function(key, output_folder){
    return function() {
        var files = publishFolder[key];
        return gulp.src('./'+files).
            pipe(gulp.dest(output_folder+path.dirname(files))).
            pipe(livereload());
    };
}

// creating copy tasks for all pubfolders
for (var key in publishFolder){
    var devcopy_task='devcopy_'+key;
    gulp.task(devcopy_task, create_copy_task_function(key, pluginFolder));
    devcopy_tasks.push(devcopy_task);

    var build_task='build_'+key;
    gulp.task(build_task, create_copy_task_function(key, buildFolder));
    build_tasks.push(build_task);
}

// cleanup plugin directory
gulp.task('clean:devcopy', function (){
    del([pluginFolder]).then(function(files) {
        process.stdout.write("deleted " + files.length.toString() + " plugin objects\n");
    });
});

// cleanup build directory
gulp.task('clean:build', function (){
    del([buildBase]).then(function(files) {
        process.stdout.write("deleted " + files.length.toString() + " build objects\n");
    });
});

// watch all directories separately
gulp.task('watch', function(){
    livereload.listen();
    for (var key in publishFolder){
        gulp.watch('./'+publishFolder[key], ['devcopy_'+key]);
    }
});

build_tasks.unshift('assets');
devcopy_tasks.unshift('assets');

gulp.task('devcopy', devcopy_tasks);
gulp.task('clean', ['clean:devcopy', 'clean:build']);
gulp.task('build', build_tasks, function(){
    return gulp.src(buildFolder+'/**/*').
        pipe(zip(pkg.name+'-'+pkg.version+'.zip')).
        pipe(gulp.dest(buildBase));
});
gulp.task('default', ['devcopy', 'watch']);

