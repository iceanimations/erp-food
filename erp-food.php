<?php
/**
 * Plugin Name: WP ERP - Office Food Ordering
 * Description: Employee Attendance Add-On for WP ERP
 * Plugin URI: http://www.iceanimations.com
 * Author: Talha Ahmed
 * Author URI: http://github.com/MagicRedDeer
 * Version: 0.1.1
 * License: UnLicensed
 * Text Domain: erp-food
 * Domain Path: languages
 *
 * Copyright (c) 2016 ICE Animations (email: info@iceanimations.com).
 * All rights reserved.
 *
 * This is an add-on for WordPress
 * http://wordpress.org/
 *
 * @package erp_hr_food
 */

// don't call the file directly.
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! function_exists( 'wp_get_post_autosave' ) ) {
    wp_die(
        esc_html_e(
            'You are not allowed to use a WordPress plugin file outside of WordPress',
            'erp-food'
        )
    );
}

use ICE\ERP\HRM\Food;

/**
 * Class ICE_ERP_HR_OfficeFood
 *
 * @author Talha Ahmed <Talha.ahmed@gmail.com>
 */
class ICE_ERP_HR_OfficeFood {

    /**
     * Plugin Version
     *
     * @var string
     */
    public $version = '0.1.1';

    /**
     * Return an instance
     *
     * Returns a singleton instance of the class ICE_ERP_HR_OfficeFood
     */
    static function init() {
        static $instance = false;
        if ( ! $instance ) {
            $instance = new ICE_ERP_HR_OfficeFood();
        }
        return $instance;
    }

    /**
     * Constructor for ICE_ERP_HR_OfficeFood
     *
     * Sets up all the appropriate hooks and actions
     */
    public function __construct() {
        // Define constants.
        $this->define_constants();
        include WPERP_FOOD_INCLUDES . '/class-install.php';
        add_action( 'erp_loaded', [ $this, 'plugin_init' ] );
    }

    /**
     * Define all the constants for the plugin
     */
    private function define_constants() {
        if ( defined( 'WPERP_FOOD_VERSION' ) ) {
            return;
        }
        define( 'WPERP_FOOD_VERSION'  , $this->version );
        define( 'WPERP_FOOD_FILE'     , __FILE__ );
        define( 'WPERP_FOOD_PATH'     , dirname( WPERP_FOOD_FILE ) );
        define( 'WPERP_FOOD_INCLUDES' , WPERP_FOOD_PATH . '/includes' );
        define( 'WPERP_FOOD_LIBS'     , WPERP_FOOD_PATH . '/includes/libs' );
        define( 'WPERP_FOOD_URL'      , plugins_url( '' , WPERP_FOOD_FILE ) );
        define( 'WPERP_FOOD_ASSETS'   , WPERP_FOOD_URL . '/assets' );
        define( 'WPERP_FOOD_CSS_LIBS' , WPERP_FOOD_ASSETS . '/css/libs' );
        define( 'WPERP_FOOD_JS_LIBS'  , WPERP_FOOD_ASSETS . '/js/libs' );
        define( 'WPERP_FOOD_VIEWS'    , WPERP_FOOD_PATH . '/views' );
        define( 'WPERP_FOOD_JS_TMPL'  , WPERP_FOOD_VIEWS . '/js-templates' );
    }

    /**
     * Instantiating classes
     */
    private function init_classes() {
        $this->food = new Food();
    }

    /**
     * Plugin_init
     */
    public function plugin_init() {
        $this->includes();
        $this->init_classes();
        $this->init_actions();
        $this->init_filters();
    }

    /**
     * Draw in all the includes
     */
    private function includes() {
        require_once( WPERP_FOOD_LIBS . '/class-tgm-plugin-activation.php' );
        require_once( WPERP_FOOD_INCLUDES . '/mealtimes.php' );
        require_once( WPERP_FOOD_INCLUDES . '/class-food.php' );
        require_once( WPERP_FOOD_INCLUDES . '/model/class-food-order.php' );
    }

    /**
     * Register all actions for this plugin
     */
    private function init_actions() {
        $this->food->init_actions();
    }

    /**
     * Register all filter hooks here.
     */
    public function init_filters() {
        $this->food->init_filters();
    }

};
global $erp_hr_food_plugin_instance;
$erp_hr_food_plugin_instance = ICE_ERP_HR_OfficeFood::init();
